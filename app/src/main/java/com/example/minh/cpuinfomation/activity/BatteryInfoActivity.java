package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class BatteryInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mBatteryLevel;
    private TextView mBatteryCapacity;
    private TextView mBatteryHealth;
    private TextView mBatteryPowerSource;
    private TextView mBatteryStatus;
    private TextView mBatteryTechnology;
    private TextView mBatteryTemperature;
    private TextView mBatteryVoltage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.battery_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBatteryLevel = (TextView) findViewById(R.id.battery_level);
        mBatteryCapacity = (TextView) findViewById(R.id.battery_capacity);
        mBatteryHealth = (TextView) findViewById(R.id.battery_health);
        mBatteryPowerSource = (TextView) findViewById(R.id.battery_power_source);
        mBatteryStatus = (TextView) findViewById(R.id.battery_status);
        mBatteryTechnology = (TextView) findViewById(R.id.battery_technology);
        mBatteryTemperature = (TextView) findViewById(R.id.battery_temperature);
        mBatteryVoltage = (TextView) findViewById(R.id.battery_voltage);

        SystemUtil.getBatteryInfo(BatteryInfoActivity.this, mBatteryLevel, mBatteryCapacity, mBatteryHealth, mBatteryPowerSource,
                mBatteryStatus, mBatteryTechnology, mBatteryTemperature, mBatteryVoltage);
    }
}
