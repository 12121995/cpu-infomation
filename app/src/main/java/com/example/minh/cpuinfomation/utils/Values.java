package com.example.minh.cpuinfomation.utils;

/**
 * Created by Minh on 4/27/2016.
 */
public class Values {
    public static final String[] LANGUAGE_SUPPORTED_ALIAS = {"en", "vi"};
    public static final String LANGUAGE = "language";
    public static final String NUMBER_OF_LANGUAGE = "number_of_language";
}
