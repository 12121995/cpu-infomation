package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class MemoryInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mMemoryRamFree;
    private TextView mMemoryRamTotal;
    private TextView mMemoryRomFree;
    private TextView mMemoryRomTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.memory_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mMemoryRamFree = (TextView) findViewById(R.id.memory_ram_free);
        mMemoryRamTotal = (TextView) findViewById(R.id.memory_ram_total);
        mMemoryRomFree = (TextView) findViewById(R.id.memory_rom_free);
        mMemoryRomTotal = (TextView) findViewById(R.id.memory_rom_total);

        SystemUtil.getMemoryInfo(MemoryInfoActivity.this, mMemoryRamFree, mMemoryRamTotal, mMemoryRomFree, mMemoryRomTotal);
    }
}
