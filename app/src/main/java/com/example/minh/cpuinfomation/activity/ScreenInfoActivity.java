package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class ScreenInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mScreenBrightness;
    private TextView mScreenDensity;
    private TextView mScreenDensityType;
    private TextView mScreenOrientation;
    private TextView mScreenRefreshRate;
    private TextView mScreenResolution;
    private TextView mScreenScreenSize;
    private TextView mScreenScreenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.screen_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mScreenBrightness = (TextView) findViewById(R.id.screen_brightness);
        mScreenDensity = (TextView) findViewById(R.id.screen_density);
        mScreenDensityType = (TextView) findViewById(R.id.screen_density_type);
        mScreenOrientation = (TextView) findViewById(R.id.screen_orientation);
        mScreenRefreshRate = (TextView) findViewById(R.id.screen_refresh_rate);
        mScreenResolution = (TextView) findViewById(R.id.screen_resolution);
        mScreenScreenSize = (TextView) findViewById(R.id.screen_screen_size);
        mScreenScreenType = (TextView) findViewById(R.id.screen_type);

        SystemUtil.getScreenInfo(ScreenInfoActivity.this, mScreenBrightness, mScreenDensity, mScreenDensityType, mScreenOrientation,
                mScreenRefreshRate, mScreenResolution, mScreenScreenSize, mScreenScreenType);
    }
}
