package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class ChipsetInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    //cpu
    private TextView mCPUModel;
    private TextView mCPUCores;
    private TextView mCPUClockSpeed;
    private TextView mCPULoadAverage;
    private TextView mCPUInstructionSet;
    private TextView mCPUGovernor;
    private TextView mCPUJavaHeap;
    // gpu
    private TextView mGPURenderer;
    private TextView mGPUVendor;
    private TextView mGPUVersion;
    // high
    private LinearLayout high;
    private TextView mGPUViewportSize;
    private TextView mGPURenderbufferSize;
    private TextView mGPUCubemapSize;
    private TextView mGPUTextureSize;
    private TextView mGPUTextureUnits;
    private TextView mGPUVertexTextures;
    private TextView mGPUCombinedTextures;
    private TextView mGPUVertexAttributes;
    private TextView mGPUVertexUniforms;
    private TextView mGPUVaryingVectors;
    private TextView mGPUFragmentUniformVectors;
    // low
    private LinearLayout low;
    private TextView mGPUViewportDimension;
    private TextView mGPUTextureSize2;
    private TextView mGPUTextureStackDepth;
    private TextView mGPUTextureUnits2;
    private TextView mGOUDepthBits;
    private TextView mGPUDepthBufferBit;
    private TextView mGPULuminance;
    private TextView mGPULuminanceAlpha;
    private TextView mGPUMaxLights;
    private TextView mGPUModelviewStackDepth;
    private TextView mGPUProjectionStackDepth;
    private TextView mGPUElementsIndices;
    private TextView mGPUElementsVertices;
    // extensions
    private TextView mGPUExtensions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chipset_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.chipset_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // cpu
        mCPUModel = (TextView) findViewById(R.id.cpu_model);
        mCPUCores = (TextView) findViewById(R.id.cpu_cores);
        mCPUClockSpeed = (TextView) findViewById(R.id.cpu_clock_speed);
        mCPULoadAverage = (TextView) findViewById(R.id.cpu_load_average);
        mCPUInstructionSet = (TextView) findViewById(R.id.cpu_instruction_set);
        mCPUGovernor = (TextView) findViewById(R.id.cpu_governor);
        mCPUJavaHeap = (TextView) findViewById(R.id.cpu_java_heap);

        // gpu
        mGPURenderer = (TextView) findViewById(R.id.gpu_renderer);
        mGPUVendor = (TextView) findViewById(R.id.gpu_vendor);
        mGPUVersion = (TextView) findViewById(R.id.gpu_version);
        // high
        high = (LinearLayout) findViewById(R.id.high);
        mGPUViewportSize = (TextView) findViewById(R.id.gpu_viewport_size);
        mGPURenderbufferSize = (TextView) findViewById(R.id.gpu_renderbuffer_size);
        mGPUCubemapSize = (TextView) findViewById(R.id.gpu_cubemap_size);
        mGPUTextureSize = (TextView) findViewById(R.id.gpu_texture_size);
        mGPUTextureUnits = (TextView) findViewById(R.id.gpu_texture_units);
        mGPUVertexTextures = (TextView) findViewById(R.id.gpu_vertex_textures);
        mGPUCombinedTextures = (TextView) findViewById(R.id.gpu_combined_textures);
        mGPUVertexAttributes = (TextView) findViewById(R.id.gpu_vertex_attributes);
        mGPUVertexUniforms = (TextView) findViewById(R.id.gpu_vertex_uniforms);
        mGPUVaryingVectors = (TextView) findViewById(R.id.gpu_varying_vectors);
        mGPUFragmentUniformVectors = (TextView) findViewById(R.id.gpu_fragment_uniform_vectors);
        // low
        low = (LinearLayout) findViewById(R.id.low);
        mGPUViewportDimension = (TextView) findViewById(R.id.gpu_viewport_dimension);
        mGPUTextureSize2 = (TextView) findViewById(R.id.gpu_texture_size_2);
        mGPUTextureStackDepth = (TextView) findViewById(R.id.gpu_texture_stack_depth);
        mGPUTextureUnits2 = (TextView) findViewById(R.id.gpu_texture_units_2);
        mGOUDepthBits = (TextView) findViewById(R.id.gpu_depth_bits);
        mGPUDepthBufferBit = (TextView) findViewById(R.id.gpu_depth_buffer_bit);
        mGPULuminance = (TextView) findViewById(R.id.gpu_luminance);
        mGPULuminanceAlpha = (TextView) findViewById(R.id.gpu_luminance_alpha);
        mGPUMaxLights = (TextView) findViewById(R.id.gpu_max_lights);
        mGPUModelviewStackDepth = (TextView) findViewById(R.id.gpu_modelview_stack_depth);
        mGPUProjectionStackDepth = (TextView) findViewById(R.id.gpu_projection_stack_depth);
        mGPUElementsIndices = (TextView) findViewById(R.id.gpu_elements_indices);
        mGPUElementsVertices = (TextView) findViewById(R.id.gpu_elements_vertices);
        // extensions
        mGPUExtensions = (TextView) findViewById(R.id.gpu_extensions);

        SystemUtil.getCPUInfo(ChipsetInfoActivity.this, mCPUModel, mCPUCores, mCPUClockSpeed, mCPULoadAverage, mCPUInstructionSet,
                mCPUGovernor, mCPUJavaHeap);

        SystemUtil.getGPUInfo(ChipsetInfoActivity.this, high, low, mGPURenderer, mGPUVendor, mGPUVersion, mGPUViewportSize, mGPURenderbufferSize,
                mGPUCubemapSize, mGPUTextureSize, mGPUTextureUnits, mGPUVertexTextures, mGPUCombinedTextures, mGPUVertexAttributes,
                mGPUVertexUniforms, mGPUVaryingVectors, mGPUFragmentUniformVectors,
                mGPUViewportDimension, mGPUTextureSize2, mGPUTextureStackDepth, mGPUTextureUnits2, mGOUDepthBits, mGPUDepthBufferBit,
                mGPULuminance, mGPULuminanceAlpha, mGPUMaxLights, mGPUModelviewStackDepth, mGPUProjectionStackDepth, mGPUElementsIndices,
                mGPUElementsVertices,
                mGPUExtensions);
    }
}
