package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.AppInfoUtil;
import com.example.minh.cpuinfomation.utils.SettingsUtil;

public class InfoAppActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;

    private TextView mTextVersion;
    private TextView mTextContact;
    private Button mButtonUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_app);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_info));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTextVersion = (TextView) findViewById(R.id.text_version);
        mTextVersion.setText(AppInfoUtil.getVersionApp(InfoAppActivity.this));

        mTextContact = (TextView) findViewById(R.id.text_contact);
        mTextContact.setOnClickListener(this);

        mButtonUpdate = (Button) findViewById(R.id.button_update);
        mButtonUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_contact:
                AppInfoUtil.connectContact(InfoAppActivity.this);
                break;
            case R.id.button_update:
                SettingsUtil.rate(InfoAppActivity.this, "https://play.google.com/store/apps/details?id=" + getPackageName());
                break;
        }
    }
}
