package com.example.minh.cpuinfomation.utils.deviceinfoutils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.os.Environment;
import android.os.StatFs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Minh on 4/19/2016.
 */
public class MemoryInfoUtil {
    // free ram
    public static long getFreeRam(Activity activity) {
        ActivityManager activityManager = (ActivityManager) activity.getSystemService("activity");
        MemoryInfo memoryInfo = new MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return Math.abs(memoryInfo.availMem);
    }
    // total ram
    public static long getTotalRam() {
        RandomAccessFile reader;
        String load;
        double totRam = 0;
        try {
            reader = new RandomAccessFile("/proc/meminfo", "r");
            load = reader.readLine();
            Pattern p = Pattern.compile("(\\d+)");
            Matcher m = p.matcher(load);
            String value = "";
            while (m.find()) {
                value = m.group(1);
            }
            reader.close();
            totRam = Double.parseDouble(value);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return (long) totRam;

    }
    // free rom
    public static long getFreeRom () {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double freeIS = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            freeIS = (stat.getBlockSizeLong() * stat.getAvailableBlocksLong());
        }
        return (long) freeIS;
    }
    // total rom
    public static long getTotalRom () {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double totalIS = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            totalIS = (stat.getBlockSizeLong() *  stat.getBlockCountLong());
        }
        return (long) totalIS;
    }
}
