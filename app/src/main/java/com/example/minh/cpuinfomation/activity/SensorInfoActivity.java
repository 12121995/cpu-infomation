package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class SensorInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mSensorAccelerometer;
    private TextView mSensorAll;
    private TextView mSensorAmbientTemperature;
    private TextView mSensorGameRotationVector;
    private TextView mSensorGeomagneticRotationVector;
    private TextView mSensorGravity;
    private TextView mSensorGyroscope;
    private TextView mSensorGyroscopeUncalibrated;
    private TextView mSensorHeartRate;
    private TextView mSensorLight;
    private TextView mSensorLinearAcceleration;
    private TextView mSensorMagneticField;
    private TextView mSensorMagneticFieldUncalibrated;
    private TextView mSensorOrientation;
    private TextView mSensorPressure;
    private TextView mSensorProximity;
    private TextView mSensorRelativeHumidity;
    private TextView mSensorRotationVector;
    private TextView mSensorSignificantMotion;
    private TextView mSensorStepCounter;
    private TextView mSensorStepDetector;
    private TextView mSensorTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.sensor_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSensorAccelerometer = (TextView) findViewById(R.id.sensor_type_accelerometer);
        mSensorAll = (TextView) findViewById(R.id.sensor_type_all);
        mSensorAmbientTemperature = (TextView) findViewById(R.id.sensor_type_ambient_temperature);
        mSensorGameRotationVector = (TextView) findViewById(R.id.sensor_type_game_rotation_vector);
        mSensorGeomagneticRotationVector = (TextView) findViewById(R.id.sensor_type_geomagnetic_rotation_vector);
        mSensorGravity = (TextView) findViewById(R.id.sensor_type_gravity);
        mSensorGyroscope = (TextView) findViewById(R.id.sensor_type_gyroscope);
        mSensorGyroscopeUncalibrated = (TextView) findViewById(R.id.sensor_type_gyroscope_uncalibrated);
        mSensorHeartRate = (TextView) findViewById(R.id.sensor_type_heart_rate);
        mSensorLight = (TextView) findViewById(R.id.sensor_type_light);
        mSensorLinearAcceleration = (TextView) findViewById(R.id.sensor_type_linear_acceleration);
        mSensorMagneticField = (TextView) findViewById(R.id.sensor_type_magnetic_field);
        mSensorMagneticFieldUncalibrated = (TextView) findViewById(R.id.sensor_type_magnetic_field_uncalibrated);
        mSensorOrientation = (TextView) findViewById(R.id.sensor_type_orientation);
        mSensorPressure = (TextView) findViewById(R.id.sensor_type_pressure);
        mSensorProximity = (TextView) findViewById(R.id.sensor_type_proximity);
        mSensorRelativeHumidity = (TextView) findViewById(R.id.sensor_type_relative_humidity);
        mSensorRotationVector = (TextView) findViewById(R.id.sensor_type_rotation_vector);
        mSensorSignificantMotion = (TextView) findViewById(R.id.sensor_type_significant_motion);
        mSensorStepCounter = (TextView) findViewById(R.id.sensor_type_step_counter);
        mSensorStepDetector = (TextView) findViewById(R.id.sensor_type_step_detector);
        mSensorTemperature = (TextView) findViewById(R.id.sensor_type_temperature);

        SystemUtil.getSensorInfo(SensorInfoActivity.this, mSensorAccelerometer, mSensorAll, mSensorAmbientTemperature,
                mSensorGameRotationVector, mSensorGeomagneticRotationVector, mSensorGravity, mSensorGyroscope,
                mSensorGyroscopeUncalibrated, mSensorHeartRate, mSensorLight, mSensorLinearAcceleration, mSensorMagneticField,
                mSensorMagneticFieldUncalibrated, mSensorOrientation, mSensorPressure, mSensorProximity, mSensorRelativeHumidity,
                mSensorRotationVector, mSensorSignificantMotion, mSensorStepCounter, mSensorStepDetector, mSensorTemperature);
    }
}
