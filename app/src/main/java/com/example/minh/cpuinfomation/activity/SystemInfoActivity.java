package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class SystemInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    // android
    private TextView mAndroidInfoVersionName;
    private TextView mAndroidInfoVersionCode;
    private TextView mAndroidInfoBootloader;
    private TextView mAndroidInfoBuildFingerprint;
    private TextView mAndroidInfoBuildID;
    private TextView mAndroidInfoBuildTime;
    private TextView mAndroidInfoCodeName;
    private TextView mAndroidInfoIncremental;
    private TextView mAndroidInfoOpenGLES;
    private TextView mAndroidInfoOpenGLVersion;
    private TextView mAndroidInfoPlatformVersion;
    private TextView mAndroidInfoRILVersion;
    private TextView mAndroidInfoSDKVersion;
    private TextView mAndroidInfoSystemEncoding;
    private TextView mAndroidInfoSystemLanguage;
    private TextView mAndroidInfoSystemOSVersion;
    private TextView mAndroidInfoSystemRegion;
    // jvm
    private TextView mJVMInfoJVMVersion;
    private TextView mJVMInfoJavaClass;
    private TextView mJVMInfoJavaHome;
    private TextView mJVMInfoJavaVendor;
    private TextView mJVMInfoJavaVM;
    private TextView mJVMInfoJVMSpecification;
    private TextView mJVMInfoJVMSpecificationVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.system_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //android
        mAndroidInfoVersionName = (TextView) findViewById(R.id.android_version_name);
        mAndroidInfoVersionCode = (TextView) findViewById(R.id.android_version_code);
        mAndroidInfoBootloader = (TextView) findViewById(R.id.android_bootloader);
        mAndroidInfoBuildFingerprint = (TextView) findViewById(R.id.android_build_fingerprint);
        mAndroidInfoBuildID = (TextView) findViewById(R.id.android_build_id);
        mAndroidInfoBuildTime = (TextView) findViewById(R.id.android_build_time);
        mAndroidInfoCodeName = (TextView) findViewById(R.id.android_code_name);
        mAndroidInfoIncremental = (TextView) findViewById(R.id.android_incremental);
        mAndroidInfoOpenGLES = (TextView) findViewById(R.id.android_opengl_es);
        mAndroidInfoOpenGLVersion = (TextView) findViewById(R.id.android_opengl_version);
        mAndroidInfoPlatformVersion = (TextView) findViewById(R.id.android_platform_version);
        mAndroidInfoRILVersion = (TextView) findViewById(R.id.android_ril_version);
        mAndroidInfoSDKVersion = (TextView) findViewById(R.id.android_sdk_version);
        mAndroidInfoSystemEncoding = (TextView) findViewById(R.id.android_system_encoding);
        mAndroidInfoSystemLanguage = (TextView) findViewById(R.id.android_system_language);
        mAndroidInfoSystemOSVersion = (TextView) findViewById(R.id.android_system_os_version);
        mAndroidInfoSystemRegion = (TextView) findViewById(R.id.android_system_region);
        // jvm
        mJVMInfoJVMVersion = (TextView) findViewById(R.id.jvm_version);
        mJVMInfoJavaClass = (TextView) findViewById(R.id.jvm_java_class_version);
        mJVMInfoJavaHome = (TextView) findViewById(R.id.jvm_java_home);
        mJVMInfoJavaVendor = (TextView) findViewById(R.id.jvm_java_vendor);
        mJVMInfoJavaVM = (TextView) findViewById(R.id.jvm_java_vm);
        mJVMInfoJVMSpecification = (TextView) findViewById(R.id.jvm_specification);
        mJVMInfoJVMSpecificationVersion = (TextView) findViewById(R.id.jvm_specification_version);

        SystemUtil.getAndroidInfo(SystemInfoActivity.this, mAndroidInfoVersionName, mAndroidInfoVersionCode,
                mAndroidInfoBootloader, mAndroidInfoBuildFingerprint, mAndroidInfoBuildID, mAndroidInfoBuildTime, mAndroidInfoCodeName,
                mAndroidInfoIncremental, mAndroidInfoOpenGLES, mAndroidInfoOpenGLVersion, mAndroidInfoPlatformVersion, mAndroidInfoRILVersion,
                mAndroidInfoSDKVersion, mAndroidInfoSystemEncoding, mAndroidInfoSystemLanguage, mAndroidInfoSystemOSVersion,
                mAndroidInfoSystemRegion);
        SystemUtil.getJVMInfo(mJVMInfoJVMVersion, mJVMInfoJavaClass, mJVMInfoJavaHome, mJVMInfoJavaVendor,
                mJVMInfoJavaVM, mJVMInfoJVMSpecification, mJVMInfoJVMSpecificationVersion);
    }
}
