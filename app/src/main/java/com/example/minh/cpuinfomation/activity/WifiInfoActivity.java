package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class WifiInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mWifiInfoSignal;
    private TextView mWifiInfoName;
    private TextView mWifiInfoIPAddress;
    private TextView mWifiInfoSpeed;
    private TextView mWifiInfoMACAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.wifi_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mWifiInfoSignal = (TextView) findViewById(R.id.wifi_signal);
        mWifiInfoName = (TextView) findViewById(R.id.wifi_name);
        mWifiInfoIPAddress = (TextView) findViewById(R.id.wifi_ip_address);
        mWifiInfoSpeed = (TextView) findViewById(R.id.wifi_speed);
        mWifiInfoMACAddress = (TextView) findViewById(R.id.wifi_mac_address);

        SystemUtil.getWifiInfo(WifiInfoActivity.this, mWifiInfoSignal, mWifiInfoName, mWifiInfoIPAddress,
                mWifiInfoSpeed, mWifiInfoMACAddress);
    }
}