package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.deviceinfoutils.CameraInfoUtil;

public class CameraInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;

    private RelativeLayout mCheckCameraRelative;
    private LinearLayout mButtonLayout;
    private TextView mCheckCameraText;
    private TextView mCameraBack;
    private TextView mCameraFront;
    // camera
    private TextView mCameraType;
    private TextView mCameraOrientation;
    private TextView mCameraAntibanding;
    private TextView mCameraAutoExposureLock;
    private TextView mCameraAutoWhiteBalanceLock;
    private TextView mCameraColorEffect;
    private TextView mCameraExposureCompensation;
    private TextView mCameraExposureCompensationStep;
    private TextView mCameraFlashMode;
    private TextView mCameraFocalLength;
    private TextView mCameraMaxNumFocusAreas;
    private TextView mCameraFocusDistancesNearIndex;
    private TextView mCameraFocusDistancesFarIndex;
    private TextView mCameraFocusDistancesOptimalIndex;
    private TextView mCameraFocusMode;
    private TextView mCameraHorizontalViewAngle;
    private TextView mCameraJpegQuality;
    private TextView mCameraJpegThumbnailQuality;
    private TextView mCameraJpegThumbnailSize;
    private TextView mCameraMaxExposureCompensation;
    private TextView mCameraMaxNumDetectedFaces;
    private TextView mCameraMaxNumMeteringAreas;
    private TextView mCameraMaxZoom;
    private TextView mCameraMinExposureCompensation;
    private TextView mCameraPictureFormat;
    private TextView mCameraPictureSize;
    private TextView mCameraPreferredPreviewSizeForVideo;
    private TextView mCameraPreviewFormat;
    private TextView mCameraMinIndex;
    private TextView mCameraMaxIndex;
    private TextView mCameraDEPRECATEDPreviewFrameRate;
    private TextView mCameraPreviewSize;
    private TextView mCameraSceneMode;
    private TextView mCameraSupportedAntibanding;
    private TextView mCameraSupportedColorEffect;
    private TextView mCameraSupportedFlashMode;
    private TextView mCameraSupportedJpegThumbnailSize;
    private TextView mCameraSupportedPictureFormat;
    private TextView mCameraSupportedPictureSize;
    private TextView mCameraSupportedPreviewFormat;
    private TextView mCameraSupportedPreviewFPSRANGEMINMAX;
    private TextView mCameraSupportedPreviewFrameRate;
    private TextView mCameraSupportedPreviewSize;
    private TextView mCameraSupportedSceneMode;
    private TextView mCameraSupportedVideoSize;
    private TextView mCameraSupportedWhiteBalance;
    private TextView mCameraVerticalViewAngle;
    private TextView mCameraVideoStabilization;
    private TextView mCameraWhiteBalance;
    private TextView mCameraZoom;
    private TextView mCameraZoomRatios;
    private TextView mCameraAutoExposureLockSupported;
    private TextView mCameraAutoWhiteBalanceLockSupported;
    private TextView mCameraSmoothZoomSupported;
    private TextView mCameraVideoSnapshotSupported;
    private TextView mCameraVideoStabilizationSupported;
    private TextView mCameraZoomSupported;
    private TextView mCameraParameterFlattenString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.camera_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mCheckCameraRelative = (RelativeLayout) findViewById(R.id.check_camera_relative);
        mButtonLayout = (LinearLayout) findViewById(R.id.button_camera);
        mCheckCameraText = (TextView) findViewById(R.id.check_camera_text);
        mCameraBack = (TextView) findViewById(R.id.camera_back);
        mCameraFront = (TextView) findViewById(R.id.camera_front);
        mCameraBack.setOnClickListener(this);
        mCameraFront.setOnClickListener(this);
        CameraInfoUtil.setLayoutCameras(CameraInfoActivity.this, mCheckCameraText, mCheckCameraRelative, mButtonLayout);

        // camera info
        mCameraType = (TextView) findViewById(R.id.camera_type);
        mCameraOrientation = (TextView) findViewById(R.id.camera_orientation);
        mCameraAntibanding = (TextView) findViewById(R.id.camera_antibanding);
        mCameraAutoExposureLock = (TextView) findViewById(R.id.camera_auto_exposure_lock);
        mCameraAutoWhiteBalanceLock = (TextView) findViewById(R.id.camera_auto_white_balance_lock);
        mCameraColorEffect = (TextView) findViewById(R.id.camera_color_effect);
        mCameraExposureCompensation = (TextView) findViewById(R.id.camera_exposure_compensation);
        mCameraExposureCompensationStep = (TextView) findViewById(R.id.camera_exposure_compensation_step);
        mCameraFlashMode = (TextView) findViewById(R.id.camera_flash_mode);
        mCameraFocalLength = (TextView) findViewById(R.id.camera_focal_length);
        mCameraMaxNumFocusAreas = (TextView) findViewById(R.id.camera_max_num_focus_areas);
        mCameraFocusDistancesNearIndex = (TextView) findViewById(R.id.camera_focus_distances_near_index);
        mCameraFocusDistancesFarIndex = (TextView) findViewById(R.id.camera_focus_distances_far_index);
        mCameraFocusDistancesOptimalIndex = (TextView) findViewById(R.id.camera_focus_distances_optimal_index);
        mCameraFocusMode = (TextView) findViewById(R.id.camera_focus_mode);
        mCameraHorizontalViewAngle = (TextView) findViewById(R.id.camera_horizontal_view_angle);
        mCameraJpegQuality = (TextView) findViewById(R.id.camera_jpeg_quality);
        mCameraJpegThumbnailQuality = (TextView) findViewById(R.id.camera_jpeg_thumbnail_quality);
        mCameraJpegThumbnailSize = (TextView) findViewById(R.id.camera_jpeg_thumbnail_size);
        mCameraMaxExposureCompensation = (TextView) findViewById(R.id.camera_max_exposure_compensation);
        mCameraMaxNumDetectedFaces = (TextView) findViewById(R.id.camera_max_num_detected_faces);
        mCameraMaxNumMeteringAreas = (TextView) findViewById(R.id.camera_max_num_metering_areas);
        mCameraMaxZoom = (TextView) findViewById(R.id.camera_max_zoom);
        mCameraMinExposureCompensation = (TextView) findViewById(R.id.camera_min_exposure_compensation);
        mCameraPictureFormat = (TextView) findViewById(R.id.camera_picture_format);
        mCameraPictureSize = (TextView) findViewById(R.id.camera_picture_size);
        mCameraPreferredPreviewSizeForVideo = (TextView) findViewById(R.id.camera_preferred_preview_size_for_video);
        mCameraPreviewFormat = (TextView) findViewById(R.id.camera_picture_format);
        mCameraMinIndex = (TextView) findViewById(R.id.camera_preview_fps_range_min_index);
        mCameraMaxIndex = (TextView) findViewById(R.id.camera_preview_fps_range_max_index);
        mCameraDEPRECATEDPreviewFrameRate = (TextView) findViewById(R.id.camera_deprecated_preview_frame_rate);
        mCameraPreviewSize = (TextView) findViewById(R.id.camera_preview_size);
        mCameraSceneMode = (TextView) findViewById(R.id.camera_scene_mode);
        mCameraSupportedAntibanding = (TextView) findViewById(R.id.camera_supported_antibanding);
        mCameraSupportedColorEffect = (TextView) findViewById(R.id.camera_supported_color_effect);
        mCameraSupportedFlashMode = (TextView) findViewById(R.id.camera_supported_flash_mode);
        mCameraSupportedJpegThumbnailSize = (TextView) findViewById(R.id.camera_supported_jpeg_thumbnail_size);
        mCameraSupportedPictureFormat = (TextView) findViewById(R.id.camera_supported_picture_format);
        mCameraSupportedPictureSize = (TextView) findViewById(R.id.camera_supported_picture_size);
        mCameraSupportedPreviewFormat = (TextView) findViewById(R.id.camera_supported_preview_format);
        mCameraSupportedPreviewFPSRANGEMINMAX = (TextView) findViewById(R.id.camera_supported_preview_fps_range_min_max);
        mCameraSupportedPreviewFrameRate = (TextView) findViewById(R.id.camera_supported_preview_frame_rate);
        mCameraSupportedPreviewSize = (TextView) findViewById(R.id.camera_supported_preview_size);
        mCameraSupportedSceneMode = (TextView) findViewById(R.id.camera_supported_scene_mode);
        mCameraSupportedVideoSize = (TextView) findViewById(R.id.camera_supported_video_size);
        mCameraSupportedWhiteBalance = (TextView) findViewById(R.id.camera_supported_white_balance);
        mCameraVerticalViewAngle = (TextView) findViewById(R.id.camera_vertical_view_angle);
        mCameraVideoStabilization = (TextView) findViewById(R.id.camera_video_stabilization);
        mCameraWhiteBalance = (TextView) findViewById(R.id.camera_white_balance);
        mCameraZoom = (TextView) findViewById(R.id.camera_zoom);
        mCameraZoomRatios = (TextView) findViewById(R.id.camera_zoom_ratios);
        mCameraAutoExposureLockSupported = (TextView) findViewById(R.id.camera_auto_exposure_lock_supported);
        mCameraAutoWhiteBalanceLockSupported = (TextView) findViewById(R.id.camera_auto_white_balance_lock_supported);
        mCameraSmoothZoomSupported = (TextView) findViewById(R.id.camera_smooth_zoom_supported);
        mCameraVideoSnapshotSupported = (TextView) findViewById(R.id.camera_video_snapshot_supported);
        mCameraVideoStabilizationSupported = (TextView) findViewById(R.id.camera_video_stabilization_supported);
        mCameraZoomSupported = (TextView) findViewById(R.id.camera_zoom_supported);
        mCameraParameterFlattenString = (TextView) findViewById(R.id.camera_parameter_flatten_string);

        CameraInfoUtil.setDefaultCamera(CameraInfoActivity.this, 0, mCameraType, mCameraOrientation, mCameraAntibanding,
                mCameraAutoExposureLock, mCameraAutoWhiteBalanceLock, mCameraColorEffect, mCameraExposureCompensation,
                mCameraExposureCompensationStep, mCameraFlashMode, mCameraFocalLength, mCameraMaxNumFocusAreas, mCameraFocusDistancesNearIndex,
                mCameraFocusDistancesFarIndex, mCameraFocusDistancesOptimalIndex, mCameraFocusMode,
                mCameraHorizontalViewAngle, mCameraJpegQuality, mCameraJpegThumbnailQuality, mCameraJpegThumbnailSize,
                mCameraMaxExposureCompensation, mCameraMaxNumDetectedFaces, mCameraMaxNumMeteringAreas, mCameraMaxZoom,
                mCameraMinExposureCompensation, mCameraPictureFormat, mCameraPictureSize, mCameraPreferredPreviewSizeForVideo,
                mCameraPreviewFormat, mCameraMinIndex, mCameraMaxIndex, mCameraDEPRECATEDPreviewFrameRate, mCameraPreviewSize,
                mCameraSceneMode, mCameraSupportedAntibanding, mCameraSupportedColorEffect, mCameraSupportedFlashMode,
                mCameraSupportedJpegThumbnailSize, mCameraSupportedPictureFormat, mCameraSupportedPictureSize, mCameraSupportedPreviewFormat,
                mCameraSupportedPreviewFPSRANGEMINMAX, mCameraSupportedPreviewFrameRate, mCameraSupportedPreviewSize,
                mCameraSupportedSceneMode, mCameraSupportedVideoSize, mCameraSupportedWhiteBalance, mCameraVerticalViewAngle,
                mCameraVideoStabilization, mCameraWhiteBalance, mCameraZoom, mCameraZoomRatios, mCameraAutoExposureLockSupported,
                mCameraAutoWhiteBalanceLockSupported, mCameraSmoothZoomSupported, mCameraVideoSnapshotSupported, mCameraVideoStabilizationSupported,
                mCameraZoomSupported, mCameraParameterFlattenString);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_back:
                CameraInfoUtil.setDefaultCamera(CameraInfoActivity.this, 0, mCameraType, mCameraOrientation, mCameraAntibanding,
                        mCameraAutoExposureLock, mCameraAutoWhiteBalanceLock, mCameraColorEffect, mCameraExposureCompensation,
                        mCameraExposureCompensationStep, mCameraFlashMode, mCameraFocalLength, mCameraMaxNumFocusAreas, mCameraFocusDistancesNearIndex,
                        mCameraFocusDistancesFarIndex, mCameraFocusDistancesOptimalIndex, mCameraFocusMode,
                        mCameraHorizontalViewAngle, mCameraJpegQuality, mCameraJpegThumbnailQuality, mCameraJpegThumbnailSize,
                        mCameraMaxExposureCompensation, mCameraMaxNumDetectedFaces, mCameraMaxNumMeteringAreas, mCameraMaxZoom,
                        mCameraMinExposureCompensation, mCameraPictureFormat, mCameraPictureSize, mCameraPreferredPreviewSizeForVideo,
                        mCameraPreviewFormat, mCameraMinIndex, mCameraMaxIndex, mCameraDEPRECATEDPreviewFrameRate, mCameraPreviewSize,
                        mCameraSceneMode, mCameraSupportedAntibanding, mCameraSupportedColorEffect, mCameraSupportedFlashMode,
                        mCameraSupportedJpegThumbnailSize, mCameraSupportedPictureFormat, mCameraSupportedPictureSize, mCameraSupportedPreviewFormat,
                        mCameraSupportedPreviewFPSRANGEMINMAX, mCameraSupportedPreviewFrameRate, mCameraSupportedPreviewSize,
                        mCameraSupportedSceneMode, mCameraSupportedVideoSize, mCameraSupportedWhiteBalance, mCameraVerticalViewAngle,
                        mCameraVideoStabilization, mCameraWhiteBalance, mCameraZoom, mCameraZoomRatios, mCameraAutoExposureLockSupported,
                        mCameraAutoWhiteBalanceLockSupported, mCameraSmoothZoomSupported, mCameraVideoSnapshotSupported, mCameraVideoStabilizationSupported,
                        mCameraZoomSupported, mCameraParameterFlattenString);
                break;
            case R.id.camera_front:
                CameraInfoUtil.setDefaultCamera(CameraInfoActivity.this, 1, mCameraType, mCameraOrientation, mCameraAntibanding,
                        mCameraAutoExposureLock, mCameraAutoWhiteBalanceLock, mCameraColorEffect, mCameraExposureCompensation,
                        mCameraExposureCompensationStep, mCameraFlashMode, mCameraFocalLength, mCameraMaxNumFocusAreas, mCameraFocusDistancesNearIndex,
                        mCameraFocusDistancesFarIndex, mCameraFocusDistancesOptimalIndex, mCameraFocusMode,
                        mCameraHorizontalViewAngle, mCameraJpegQuality, mCameraJpegThumbnailQuality, mCameraJpegThumbnailSize,
                        mCameraMaxExposureCompensation, mCameraMaxNumDetectedFaces, mCameraMaxNumMeteringAreas, mCameraMaxZoom,
                        mCameraMinExposureCompensation, mCameraPictureFormat, mCameraPictureSize, mCameraPreferredPreviewSizeForVideo,
                        mCameraPreviewFormat, mCameraMinIndex, mCameraMaxIndex, mCameraDEPRECATEDPreviewFrameRate, mCameraPreviewSize,
                        mCameraSceneMode, mCameraSupportedAntibanding, mCameraSupportedColorEffect, mCameraSupportedFlashMode,
                        mCameraSupportedJpegThumbnailSize, mCameraSupportedPictureFormat, mCameraSupportedPictureSize, mCameraSupportedPreviewFormat,
                        mCameraSupportedPreviewFPSRANGEMINMAX, mCameraSupportedPreviewFrameRate, mCameraSupportedPreviewSize,
                        mCameraSupportedSceneMode, mCameraSupportedVideoSize, mCameraSupportedWhiteBalance, mCameraVerticalViewAngle,
                        mCameraVideoStabilization, mCameraWhiteBalance, mCameraZoom, mCameraZoomRatios, mCameraAutoExposureLockSupported,
                        mCameraAutoWhiteBalanceLockSupported, mCameraSmoothZoomSupported, mCameraVideoSnapshotSupported, mCameraVideoStabilizationSupported,
                        mCameraZoomSupported, mCameraParameterFlattenString);
                break;
        }
    }
}
