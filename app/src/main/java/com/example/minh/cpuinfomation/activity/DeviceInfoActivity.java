package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class DeviceInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mDeviceAndroidID;
    private TextView mDeviceBoard;
    private TextView mDeviceBrand;
    private TextView mDeviceBuildTime;
    private TextView mDeviceCPUABI;
    private TextView mDeviceCPUABI2;
    private TextView mDeviceDevice;
    private TextView mDeviceDeviceSerial;
    private TextView mDeviceDisplay;
    private TextView mDeviceHardware;
    private TextView mDeviceHost;
    private TextView mDeviceICCCard;
    private TextView mDeviceIMEI;
    private TextView mDeviceKernel;
    private TextView mDeviceManufacturer;
    private TextView mDeviceModel;
    private TextView mDeviceProduct;
    private TextView mDevicePhonenumber;
    private TextView mDeviceRelease;
    private TextView mDeviceRoot;
    private TextView mDeviceSIMSerial;
    private TextView mDeviceSubcriberID;
    private TextView mDeviceTags;
    private TextView mDeviceType;
    private TextView mDeviceUptime;
    private TextView mDeviceUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.device_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mDeviceAndroidID = (TextView) findViewById(R.id.device_android_id);
        mDeviceBoard = (TextView) findViewById(R.id.device_board);
        mDeviceBrand = (TextView) findViewById(R.id.device_brand);
        mDeviceBuildTime = (TextView) findViewById(R.id.device_build_time);
        mDeviceCPUABI = (TextView) findViewById(R.id.device_cpu_abi);
        mDeviceCPUABI2 = (TextView) findViewById(R.id.device_cpu_abi_2);
        mDeviceDevice = (TextView) findViewById(R.id.device_device);
        mDeviceDeviceSerial = (TextView) findViewById(R.id.device_device_serial);
        mDeviceDisplay = (TextView) findViewById(R.id.device_display);
        mDeviceHardware = (TextView) findViewById(R.id.device_hardware);
        mDeviceHost = (TextView) findViewById(R.id.device_host);
        mDeviceICCCard = (TextView) findViewById(R.id.device_icc_card);
        mDeviceIMEI = (TextView) findViewById(R.id.device_imei);
        mDeviceKernel = (TextView) findViewById(R.id.device_kernel);
        mDeviceManufacturer = (TextView) findViewById(R.id.device_manufacturer);
        mDeviceModel = (TextView) findViewById(R.id.device_model);
        mDeviceProduct = (TextView) findViewById(R.id.device_product);
        mDevicePhonenumber = (TextView) findViewById(R.id.device_phone_number);
        mDeviceRelease = (TextView) findViewById(R.id.device_release);
        mDeviceRoot = (TextView) findViewById(R.id.device_root);
        mDeviceSIMSerial = (TextView) findViewById(R.id.device_sim_serial);
        mDeviceSubcriberID = (TextView) findViewById(R.id.device_subcriber_id);
        mDeviceTags = (TextView) findViewById(R.id.device_tags);
        mDeviceType = (TextView) findViewById(R.id.device_type);
        mDeviceUptime = (TextView) findViewById(R.id.device_uptime);
        mDeviceUser = (TextView) findViewById(R.id.device_user);

        SystemUtil.getDeviceInfo(DeviceInfoActivity.this, mDeviceAndroidID, mDeviceBoard, mDeviceBrand,
                mDeviceBuildTime, mDeviceCPUABI, mDeviceCPUABI2, mDeviceDevice, mDeviceDeviceSerial, mDeviceDisplay, mDeviceHardware,
                mDeviceHost, mDeviceICCCard, mDeviceIMEI, mDeviceKernel, mDeviceManufacturer, mDeviceModel, mDeviceProduct, mDevicePhonenumber,
                mDeviceRelease, mDeviceRoot, mDeviceSIMSerial, mDeviceSubcriberID, mDeviceTags, mDeviceType, mDeviceUptime, mDeviceUser);
    }
}
