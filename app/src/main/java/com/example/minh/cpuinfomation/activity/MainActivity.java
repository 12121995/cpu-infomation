package com.example.minh.cpuinfomation.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SettingsUtil;
import com.example.minh.cpuinfomation.utils.Values;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout mRelativeChipset;
    private RelativeLayout mRelativeSystem;
    private RelativeLayout mRelativeDevice;
    private RelativeLayout mRelativeWifi;
    private RelativeLayout mRelativeNetwork;
    private RelativeLayout mRelativeBattery;
    private RelativeLayout mRelativeScreen;
    private RelativeLayout mRelativeCamera;
    private RelativeLayout mRelativeMemory;
    private RelativeLayout mRelativeSensor;

    private boolean mCheckPermissonREADPHONESTATE = false;
    private boolean mCheckPermissonCAMERA = false;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SettingsUtil.setLanguage(MainActivity.this, sharedPreferences.getString(Values.LANGUAGE, "en"));
        setContentView(R.layout.activity_main);

        mRelativeChipset = (RelativeLayout) findViewById(R.id.chipset_info);
        mRelativeSystem = (RelativeLayout) findViewById(R.id.system_info);
        mRelativeDevice = (RelativeLayout) findViewById(R.id.device_info);
        mRelativeWifi = (RelativeLayout) findViewById(R.id.wifi_info);
        mRelativeNetwork = (RelativeLayout) findViewById(R.id.network_info);
        mRelativeBattery = (RelativeLayout) findViewById(R.id.battery_info);
        mRelativeScreen = (RelativeLayout) findViewById(R.id.screen_info);
        mRelativeCamera = (RelativeLayout) findViewById(R.id.camera_info);
        mRelativeMemory = (RelativeLayout) findViewById(R.id.memory_info);
        mRelativeSensor = (RelativeLayout) findViewById(R.id.sensor_info);
        if (mRelativeChipset != null) {
            mRelativeChipset.setOnClickListener(this);
        }
        if (mRelativeSystem != null) {
            mRelativeSystem.setOnClickListener(this);
        }
        if (mRelativeDevice != null) {
            mRelativeDevice.setOnClickListener(this);
        }
        if (mRelativeNetwork != null) {
            mRelativeNetwork.setOnClickListener(this);
        }
        if (mRelativeWifi != null) {
            mRelativeWifi.setOnClickListener(this);
        }
        if (mRelativeBattery != null) {
            mRelativeBattery.setOnClickListener(this);
        }
        if (mRelativeScreen != null) {
            mRelativeScreen.setOnClickListener(this);
        }
        if (mRelativeCamera != null) {
            mRelativeCamera.setOnClickListener(this);
        }
        if (mRelativeMemory != null) {
            mRelativeMemory.setOnClickListener(this);
        }
        if (mRelativeSensor != null) {
            mRelativeSensor.setOnClickListener(this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA}, 1212);

                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonREADPHONESTATE = true;
                }
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonCAMERA = true;
                }

            }
        } else {
            mCheckPermissonREADPHONESTATE = true;
            mCheckPermissonCAMERA = true;
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            mCheckPermissonREADPHONESTATE = true;
        }
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            mCheckPermissonCAMERA = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1212:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonREADPHONESTATE = true;
                }
                if (grantResults.length > 0 && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonCAMERA = true;
                }
                break;
            case  19951:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonREADPHONESTATE = true;
                }
                break;
            case  19952:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCheckPermissonCAMERA = true;
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chipset_info:
                startActivity(new Intent(MainActivity.this, ChipsetInfoActivity.class));
                break;
            case R.id.system_info:
                startActivity(new Intent(MainActivity.this, SystemInfoActivity.class));
                break;
            case R.id.device_info:
                if (mCheckPermissonREADPHONESTATE) {
                    startActivity(new Intent(MainActivity.this, DeviceInfoActivity.class));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 19951);
                        }
                    }
                }
                break;
            case R.id.wifi_info:
                startActivity(new Intent(MainActivity.this, WifiInfoActivity.class));
                break;
            case R.id.network_info:
                if (mCheckPermissonREADPHONESTATE) {
                    startActivity(new Intent(MainActivity.this, NetworkInfoActivity.class));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 19951);
                        }
                    }
                }
                break;
            case R.id.battery_info:
                startActivity(new Intent(MainActivity.this, BatteryInfoActivity.class));
                break;
            case R.id.screen_info:
                startActivity(new Intent(MainActivity.this, ScreenInfoActivity.class));
                break;
            case R.id.camera_info:
                if (mCheckPermissonCAMERA) {
                    startActivity(new Intent(MainActivity.this, CameraInfoActivity.class));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, 19952);
                        }
                    }
                }
                break;
            case R.id.memory_info:
                startActivity(new Intent(MainActivity.this, MemoryInfoActivity.class));
                break;
            case R.id.sensor_info:
                startActivity(new Intent(MainActivity.this, SensorInfoActivity.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_rate:
                SettingsUtil.rate(MainActivity.this, "http://play.google.com/store/apps/details?id=" + getPackageName());
                break;
            case R.id.action_share:
                SettingsUtil.share(MainActivity.this, "http://play.google.com/store/apps/details?id=" + getPackageName());
                break;
            case R.id.action_language:
                SettingsUtil.changeLanguage(MainActivity.this);
                break;
            case R.id.action_about:
                startActivity(new Intent(MainActivity.this, InfoAppActivity.class));
                break;
            case R.id.action_more:
                SettingsUtil.more(MainActivity.this);
                break;
        }
        return true;
    }
}
