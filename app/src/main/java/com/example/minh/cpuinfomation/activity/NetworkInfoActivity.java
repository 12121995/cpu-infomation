package com.example.minh.cpuinfomation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;
import com.example.minh.cpuinfomation.utils.SystemUtil;

public class NetworkInfoActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private TextView mNetworkInfoSignal;
    private TextView mNetworkInfoBaseBand;
    private TextView mNetworkInfoCountry;
    private TextView mNetworkInfoType;
    private TextView mNetworkInfoOperatorID;
    private TextView mNetworkInfoOperatorName;
    private TextView mNetworkInfoPhoneType;
    private TextView mNetworkInfoServiceMode;
    private TextView mNetworkInfoSim;
    private TextView mNetworkInfoSimSerial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_info);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.sim_information));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mNetworkInfoSignal = (TextView) findViewById(R.id.network_signal);
        mNetworkInfoBaseBand = (TextView) findViewById(R.id.network_base_band);
        mNetworkInfoCountry = (TextView) findViewById(R.id.network_country);
        mNetworkInfoType = (TextView) findViewById(R.id.network_network_type);
        mNetworkInfoOperatorID = (TextView) findViewById(R.id.network_operator_id);
        mNetworkInfoOperatorName = (TextView)findViewById(R.id.network_operator_name);
        mNetworkInfoPhoneType = (TextView) findViewById(R.id.network_phone_type);
        mNetworkInfoServiceMode = (TextView) findViewById(R.id.network_service_mode);
        mNetworkInfoSim = (TextView) findViewById(R.id.network_sim);
        mNetworkInfoSimSerial = (TextView) findViewById(R.id.network_sim_serial);

        SystemUtil.getNetworkInfo(NetworkInfoActivity.this, mNetworkInfoSignal, mNetworkInfoBaseBand,
                mNetworkInfoCountry, mNetworkInfoType, mNetworkInfoOperatorID, mNetworkInfoOperatorName, mNetworkInfoPhoneType,
                mNetworkInfoServiceMode, mNetworkInfoSim, mNetworkInfoSimSerial);
    }
}
