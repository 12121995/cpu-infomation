package com.example.minh.cpuinfomation.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.cpuz.cpuinfo.systeminfo.deviceid.R;


/**
 * Created by Minh on 4/27/2016.
 */
public class AppInfoUtil {
    // version app
    public static String getVersionApp (Activity activity) {
        try {
            return activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    // contact
    public static void connectContact (Activity activity) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{activity.getString(R.string.gpaddy_contact)});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
}
