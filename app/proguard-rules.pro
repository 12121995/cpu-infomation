####################################################################################################
####################################################################################################
####################################################################################################
######################################### PROGUARD #################################################
####################################################################################################
####################################################################################################
####################################################################################################

# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
#-dontoptimize
#-dontpreverify

# If you want to enable optimization, you should include the
# following:
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
#
# Note that you cannot just include these flags in your own
# configuration file; if you are including this file, optimization
# will be turned off. You'll need to either edit this file, or
# duplicate the contents of this file and remove the include of this
# file from your project's proguard.config path property.

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends android.support.v4.app.DialogFragment
-keep public class * extends com.actionbarsherlock.app.SherlockListFragment
-keep public class * extends com.actionbarsherlock.app.SherlockFragment
-keep public class * extends com.actionbarsherlock.app.SherlockFragmentActivity
-keep public class * extends android.app.Fragment
-keep public class com.android.vending.licensing.ILicensingService

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
 native <methods>;
}

-keep public class * extends android.view.View {
 public <init>(android.content.Context);
 public <init>(android.content.Context, android.util.AttributeSet);
 public <init>(android.content.Context, android.util.AttributeSet, int);
 public void set*(...);
}

-keepclasseswithmembers class * {
 public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
 public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
 public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
 public static **[] values();
 public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
 public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
 public static <fields>;
}

#-keep class android.support.v4.** { *; }
#-keep interface android.support.v4.** { *; }
#-keep class android.support.v7.** { *; }
#-keep interface android.support.v7.** { *; }


# Allow obfuscation of android.support.v7.internal.view.menu.**
# to avoid problem on Samsung 4.2.2 devices with appcompat v21
# see https://code.google.com/p/android/issues/detail?id=78377
-keep class !android.support.v7.internal.view.menu.**,** {*;}
-keep class !android.support.v7.internal.view.menu.**,android.support.** {*;}


-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }
-keepattributes InnerClasses, EnclosingMethod
-keep class com.ironsource.mobilcore.**{ *; }

-keep public class com.google.android.gms.ads.** {
   public *;
}

-keep public class com.google.ads.** {
   public *;
}


-keep class org.askerov.dynamicgrid.** { *; }
-keep interface org.askerov.dynamicgrid.** { *; }
-keep class com.readystatesoftware.** { *; }
-keep interface com.readystatesoftware.** { *; }
-keep class com.googlecode.** { *; }
-keep interface com.googlecode.** { *; }
-keep class android.os.** { *; }
-keep class com.facebook.** { *; }
-keep class org.jsoup.** { *; }
-keep interface org.jsoup.** { *; }
-keep class com.google.** { *; }
-keepattributes Signature
-keep class org.apache.commons.codec.** { *; }
-keep interface org.apache.commons.codec.** { *; }

-keep class butterknife.** { *; }
-keep interface butterknife.** { *; }

-keep class de.greenrobot.dao.** { *; }
-keep interface de.greenrobot.dao.** { *; }

-keep class com.facebook.** { *; }
-keep interface com.facebook.** { *; }

-keepclassmembers class * extends de.greenrobot.dao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

-keep class com.google.** { *; }
-keep interface com.google.** { *; }

-keep class org.apache.http.entity.mime.** { *; }
-keep interface org.apache.http.entity.mime.** { *; }

-keep class org.apache.commons.codec.** { *; }
-keep interface org.apache.commons.codec.** { *; }


-keep class com.hp.hpl.sparta.** { *; }
-keep interface com.hp.hpl.sparta.** { *; }

-keep class net.sourceforge.pinyin4j.** { *; }
-keep interface net.sourceforge.pinyin4j.** { *; }


-keep class demo.** { *; }

-keep class com.android.volley.** { *; }
-keep interface com.android.volley.** { *; }

-keep class com.gc.materialdesign.** { *; }
-keep interface com.gc.materialdesign.** { *; }

-keep class uk.co.senab.photoview.** { *; }
-keep interface uk.co.senab.photoview.** { *; }

-keep class uk.co.senab.photoview.** { *; }
-keep interface uk.co.senab.photoview.** { *; }

-keep class com.nineoldandroids.** { *; }
-keep interface com.nineoldandroids.** { *; }

-keep class com.nostra13.universalimageloader.** { *; }
-keep interface com.nostra13.universalimageloader.** { *; }

-keep class kankan.wheel.widget.** { *; }
-keep interface kankan.wheel.widget.** { *; }

-keep class com.umeng.fb.** { *; }
-keep interface com.umeng.fb.** { *; }

-keep class com.gpaddy.widget.** { *; }
-keep interface com.gpaddy.widget.** { *; }

-keep class android.content.pm.** { *; }
-keep interface android.content.pm.** { *; }

-keep class android.os.IInterface.** { *; }
-keep interface android.os.IInterface.** { *; }

-keep class cn.pedant.SweetAlert.** { *; }
-keep interface cn.pedant.SweetAlert.** { *; }



-dontnote "!DuplicateClassPrinter*"
-dontwarn demo.**
-dontwarn com.umeng.**
-dontwarn butterknife.**


# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn com.google.ads.**


-keep class org.askerov.dynamicgrid.** { *; }
-keep interface org.askerov.dynamicgrid.** { *; }
-keep class com.readystatesoftware.** { *; }
-keep interface com.readystatesoftware.** { *; }
-keep class com.googlecode.** { *; }
-keep interface com.googlecode.** { *; }
-keep class android.os.** { *; }
-keep class com.facebook.** { *; }
-keep class org.jsoup.** { *; }
-keep interface org.jsoup.** { *; }
-keep class com.google.** { *; }
-keepattributes Signature
-keep class org.apache.commons.codec.** { *; }
-keep interface org.apache.commons.codec.** { *; }

-keep class butterknife.** { *; }
-keep interface butterknife.** { *; }

-keep class de.greenrobot.dao.** { *; }
-keep interface de.greenrobot.dao.** { *; }

-keepclassmembers class * extends de.greenrobot.dao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

-keep class com.google.** { *; }
-keep interface com.google.** { *; }

-keep class org.apache.http.entity.mime.** { *; }
-keep interface org.apache.http.entity.mime.** { *; }

-keep class org.apache.commons.codec.** { *; }
-keep interface org.apache.commons.codec.** { *; }


-keep class com.hp.hpl.sparta.** { *; }
-keep interface com.hp.hpl.sparta.** { *; }

-keep class net.sourceforge.pinyin4j.** { *; }
-keep interface net.sourceforge.pinyin4j.** { *; }


-keep class demo.** { *; }

-keep class com.android.volley.** { *; }
-keep interface com.android.volley.** { *; }

-keep class com.gc.materialdesign.** { *; }
-keep interface com.gc.materialdesign.** { *; }

-keep class uk.co.senab.photoview.** { *; }
-keep interface uk.co.senab.photoview.** { *; }

-keep class uk.co.senab.photoview.** { *; }
-keep interface uk.co.senab.photoview.** { *; }

-keep class com.nineoldandroids.** { *; }
-keep interface com.nineoldandroids.** { *; }

-keep class com.nostra13.universalimageloader.** { *; }
-keep interface com.nostra13.universalimageloader.** { *; }

-keep class kankan.wheel.widget.** { *; }
-keep interface kankan.wheel.widget.** { *; }

-keep class com.umeng.fb.** { *; }
-keep interface com.umeng.fb.** { *; }

-keep class com.bumptech.glide.** { *; }
-keep interface com.bumptech.glide.** { *; }

-keep class com.github.fabtransitionactivity.** { *; }
-keep interface com.github.fabtransitionactivity.** { *; }

-keep class com.github.jlmd.animatedcircleloadingview.** { *; }
-keep interface com.github.jlmd.animatedcircleloadingview.** { *; }

-keep class com.alertdialogpro.** { *; }
-keep interface com.alertdialogpro.** { *; }

-keep class com.alertdialogpro.material.** { *; }
-keep interface com.alertdialogpro.material.** { *; }

-keep class com.cleveroad.androidmanimation.** { *; }
-keep interface com.cleveroad.androidmanimation.** { *; }

-keep class com.daimajia.numberprogressbar.** { *; }
-keep interface com.daimajia.numberprogressbar.** { *; }

-dontnote "!DuplicateClassPrinter*"
-dontwarn demo.**
-dontwarn com.umeng.**
-dontwarn butterknife.**
-dontwarn net.rdrei.android.dirchooser.**


# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn com.google.ads.**
-dontwarn com.github.fabtransitionactivity.**
-dontwarn com.sun.activation.viewers.**
-dontwarn javax.activation.**
-dontwarn org.apache.log4j.**
-dontwarn javax.swing.**